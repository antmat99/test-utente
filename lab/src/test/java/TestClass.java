import org.junit.Test;
import java.io.IOException;
import static org.junit.Assert.assertEquals;


public class TestClass {

    @Test
    public void test() throws IOException {
        Adder a = new Adder(2,3);
        assertEquals(5, a.add());
    }

}
